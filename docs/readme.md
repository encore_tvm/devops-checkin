# DevOps Sample program in Go

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

This repository contains a simple program written in Golang named pinger. This service reponds with "hello world" at the root path and can be configured to ping another server through the environment variables (see the file at ./cmd/pinger/config.go). By default, running it will make it start a server and ping itself.


- ✨ ✨

## Directory structure

| Directory | Description |
| --- | --- |
| `/bin` | Contains binaries |
| `/cmd` | Contains source code for CLI interfaces |
| `/deployments` | Contains image files and manifests for deployments |
| `/docs` | Contains documentation |
| `/vendor` | Contains dependencies (use `make dep` to populate it) |


## Usage

Clone the respository and run the commands as per the below
- Containerisation  - docker build -f ./deployments/build/Dockerfile -t devops/pinger:latest .
- Pipeline - Run CI/CD pipeline from Gitlab
- Environment - docker-compose up -f ./deployments/docker-compose.yml





